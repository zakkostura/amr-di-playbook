# Tools Register
Where applicable all shareable tools in a project, and or the project itself should be registered in the Arup tools register. This document instructs how to do that.

Jira Issue:[DIG-42](https://arupdigital.atlassian.net/browse/DIG-42)