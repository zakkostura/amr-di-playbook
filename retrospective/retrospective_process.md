# Project Retrospective Process
This is a document outlining the process for a project review using the agile retrospective process. It also includes a project review which invokes the project architecture review, security review, KPI review and others in the Playbook.

Jira Issue:[DIG-40](https://arupdigital.atlassian.net/browse/DIG-40)