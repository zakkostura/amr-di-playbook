# Americas Digital Insight Playbook
This playbook will cover the internal standards of Digital Insight projects in Americas region regarding the following topics, Solution Design, Development, Retrospective and Test. 

## Table of contents

1. [Introduction](Introduction.md)

2. [Design Research ](design_research/README.md)

3. [Development](development/README.md)

4. [Test](test/README.md)

5. [Retrospective](retrospective/README.md)


