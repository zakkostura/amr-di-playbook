# Design Research
This section defines the design phase concerns of digital insight projects. These can range from UX tasks to systems architecture and deployment.

## Discovery 

* [User Requirements Gathering](user_requirements.md)

* [User Interviews](user_interviews.md)


## Define

* [User Story Mapping](user_story.md)

* [Decision Log](decision_log.md)

* [Meeting Notes](meeting_notes.md)

* [Project Documentation Templates](project_documentation_templates.md)

* [Project Documentation](project_documentation.md)

* [Due Diligence](due_dillgence.md)

* [Hours Estimation](hours_estimation.md)


## Design

* [User Experience Design](user_experience.md)

* [Phases of UX](phases_ux.md)


## Implementation

* [Data Architecture](data_architecture.md)

* [Software Architecture](software_architecture.md)

* [System Architecture](system_architecture.md)

* [Architecture Methodology](architecture_methodology.md)


## Release

* [Deployment Plan](deployment_plan.md)







