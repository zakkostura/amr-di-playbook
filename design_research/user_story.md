#User Story Mapping
User stories are developed from user interviews and research. User Stories drive requirements. Requirements drive design. This document describes methodology for user story mapping.

Jira Issue: [DIG-18](https://arupdigital.atlassian.net/browse/DIG-18)