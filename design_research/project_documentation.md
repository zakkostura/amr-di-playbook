#Project Documentation
This document outlines the documentation required at each phase of an insight project, the formats expected and services and software used to create those documents. They range from UX research documentation to test and deploy documentation. This document should not list what goes in each document, but should link to documentation in the playbook where there might be more detail on each document type.

Jira Issue:[DIG-20](https://arupdigital.atlassian.net/browse/DIG-20)