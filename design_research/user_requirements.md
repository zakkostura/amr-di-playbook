#User Requirements Gathering
This document is a sibling of other documents which make up the user experience design process. The process of requirements gathering should be allowed to function alone however as not all projects can afford an entire user experience design phase. Gathering and tracking requirements is the bare minimum user experience design process.

Jira Issue: [DIG-14](https://arupdigital.atlassian.net/browse/DIG-14)