#Introduction
This page will explain about who we are, what we are doing and our team's business considerations.

## About Us
This section will cover the definition of our team, mission statement and team structure.

Jira Issue: [DIG-1](https://arupdigital.atlassian.net/browse/DIG-1)


## Business Consideration
This section will define business functions of the team, service offerings and intellectual property.

Jira Issue: [DIG-2](https://arupdigital.atlassian.net/browse/DIG-2)